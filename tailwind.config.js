const windmill = require("@windmill/react-ui/config");

module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
    "./containers/**/*.{js,ts,jsx,tsx}",
    "node_modules/@windmill/react-ui/lib/defaultTheme.js",
    "node_modules/@windmill/react-ui/dist/index.js",
  ],
  darkMode: "class",

  theme: {
    extend: {},
  },
  variants: {
    backgroundOpacity: ["responsive", "hover", "focus", "dark"],
    backgroundColor: ["responsive", "hover", "focus", "active", "odd", "dark"],
    display: ["responsive", "dark"],
    textColor: [
      "responsive",
      "focus",
      "focus-within",
      "hover",
      "active",
      "dark",
    ],
    placeholderColor: ["responsive", "focus", "dark"],
    borderColor: ["responsive", "hover", "focus", "dark"],
    divideColor: ["responsive", "dark"],
    boxShadow: ["responsive", "hover", "focus", "dark"],
    margin: ["responsive", "last"],
  },
  plugins: [require('@tailwindcss/forms'), require('@tailwindcss/typography')],
};
