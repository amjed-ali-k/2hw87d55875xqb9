import { Badge } from "@windmill/react-ui";
import Image from "next/image";
import React from "react";
import { AiOutlineUser } from "react-icons/ai";
import { useFetchPermenantData } from "../../lib/adapters/fetcher";
import { PublicUserType } from "../../lib/types/db";

function UserCard({ id, badge }: { id?: string; badge?: string }) {
  const { data } = useFetchPermenantData<PublicUserType>(`/api/users/${id}`);
  if (!data || !id) return <div>Loading...</div>;
  const image = data.image
  return (
    <div className="flex items-center p-2 pl-0 text-sm">
      {!image && <div className="p-2 mr-2 rounded-full dark:bg-slate-600 bg-slate-200">
        <AiOutlineUser size={25} />
      </div>}
      {image && (
        <div className="relative w-12 h-12 mr-2 overflow-hidden rounded-full">
          <Image src={image} layout="fill" objectFit="cover" alt="Profile image" />
        </div>
      )}
      <div>
        <p className="font-semibold">
          {data?.name} {badge && <Badge>{badge}</Badge>}
        </p>
        <p className="text-xs capitalize">{data && data.designation ? data.designation.replace(/_/, " ") : data.department}</p>
      </div>
    </div>
  );
}

export default UserCard;
