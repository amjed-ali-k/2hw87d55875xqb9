import { HelperText, Label, Select } from "@windmill/react-ui";
import React from "react";
import { useField } from "formik";
import { SelectProps } from "@windmill/react-ui/dist/Select";

interface Props extends SelectProps {
  label: React.ReactText;
  name: string;
}

function SelectInput({ label, ...props }: Props) {
  const [field, meta] = useField({ ...props });
  return (
    <>
      <Label className="mt-4">
        <span>{label}</span>
        <Select
          css=""
          className="mt-1"
          {...field}
          {...props}
        />
      </Label>
      {meta.touched && meta.error ? (
        <HelperText valid={false}>{meta.error}</HelperText>
      ) : null}
    </>
  );
}

export default SelectInput;
