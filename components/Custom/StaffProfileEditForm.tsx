import React from "react";
import * as Yup from "yup";
import toast, { Toaster } from "react-hot-toast";
import { Form, Formik } from "formik";

import Title from "../Ui/Title";
import httpClient from "../../lib/helpers/httpClient";
import FormButton from "../Forms/FormButton";
import SelectInput from "../Forms/SelectInput";
import TextInput from "../Forms/TextInput";
import { staffProfileSchema } from "../../lib/schemas/profile";
import { useFetchPermenantData } from "../../lib/adapters/fetcher";
import { AuthSettings, SettingsType } from "../../lib/constants/settings";

interface InitialValues {
  designation?: string;
  department?: string;
  pen_no?: string;
  institution?: string;
  email?: string;
}

interface Props {
  initialValues?: InitialValues;
  onSuccess?: () => Promise<void>;
  email?: string;
}

const _initialValues = {
  designation: "",
  department: "",
  pen_no: "",
  institution: "",
  email: "",
  type: "staff",
};

function StaffProfileEditForm({
  initialValues = _initialValues,
  onSuccess,
  email,
}: Props) {
  if (email) initialValues.email = email;
  const { data: settings } = useFetchPermenantData<AuthSettings>(
    `/api/settings/auth`
  );
  return (
    <Formik
      initialValues={{
        ...initialValues,
      }}
      validationSchema={Yup.object(staffProfileSchema)}
      enableReinitialize
      onSubmit={async (values, { setSubmitting }) => {
        try {
          await httpClient.post("/api/users/profile/staff/", values);
          toast.success("Profile successfully updated!");
          onSuccess && await onSuccess();
        } catch (error) {
          toast.error("An error occured!");
        }
        setSubmitting(false);
      }}
    >
      <Form>
        <Toaster />
        <Title>Profile Details</Title>
        <div className="flex flex-wrap mb-8">
          <MediumBlock>
            <SelectInput name="designation" label="Designation">
            <option>Select one</option>
              {settings?.designations.map((designation) => (
                <option key={designation} value={designation}>
                  {designation.replace(/_/g, " ")}
                </option>
              ))}
            </SelectInput>
          </MediumBlock>
          <MediumBlock>
            <SelectInput name="department" label="Department">
            <option>Select one</option>
              {settings?.departments.map((department) => (
                <option key={department} value={department}>
                  {department.replace(/_/g, " ")}
                </option>
              ))}
            </SelectInput>
          </MediumBlock>
          <MediumBlock>
            <TextInput name="pen_no" type="text" label="Pen No" />
          </MediumBlock>
          <MediumBlock>
            <TextInput name="institution" type="text" label="Institution" />
          </MediumBlock>
        </div>
        <div className="flex max-w-sm mb-4 ml-2">
          <FormButton>Update Profile</FormButton>
        </div>
      </Form>
    </Formik>
  );
}

export default StaffProfileEditForm;

function MediumBlock({
  children,
  block = false,
}: {
  children: React.ReactNode;
  block?: boolean;
}) {
  const comp = (
    <div className="w-full px-2 my-2 lg:w-1/2 2xl:w-1/3">{children}</div>
  );

  return block ? <div className="w-full">{comp}</div> : comp;
}
