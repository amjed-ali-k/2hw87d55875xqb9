import React from "react";
import * as Yup from "yup";
import toast, { Toaster } from "react-hot-toast";
import { Form, Formik } from "formik";

import Title from "../Ui/Title";
import httpClient from "../../lib/helpers/httpClient";
import FormButton from "../Forms/FormButton";
import SelectInput from "../Forms/SelectInput";
import TextInput from "../Forms/TextInput";
import { studentProfileSchema } from "../../lib/schemas/profile";
import { toastError } from "../../lib/helpers/toastError";
import { useFetchPermenantData } from "../../lib/adapters/fetcher";
import { AuthSettings } from "../../lib/constants/settings";

interface InitialValues {
  department?: string;
  semester?: string;
  year_of_passout?: string;
  register_no?: string;
  institution?: string;
  email?: string;
}

interface Props {
  initialValues?: InitialValues;
  onSuccess?: () => Promise<void>;
  email?: string;
}

const _initialValues = {
  department: "",
  semester: "",
  year_of_passout: "",
  register_no: "",
  institution: "",
  type: "student",
};

function StudentProfileEditForm({
  initialValues = _initialValues,
  onSuccess,
  email,
}: Props) {
  if (email) initialValues.email = email;
  const { data: settings } = useFetchPermenantData<AuthSettings>(
    `/api/settings/auth`
  );
  return (
    <Formik
      initialValues={{
        ...initialValues,
      }}
      validationSchema={Yup.object(studentProfileSchema)}
      enableReinitialize
      onSubmit={async (values, { setSubmitting }) => {
        try {
          await httpClient.post("/api/users/profile/student/", values);
          toast.success("Profile successfully updated!");
          onSuccess && await onSuccess();
        } catch (error) {
          toastError(error);
        }
        setSubmitting(false);
      }}
    >
      <Form>
        <Toaster />
        <Title>Profile Details</Title>
        <div className="flex flex-wrap">
          <MediumBlock>
            <SelectInput name="department" label="Department">
              <option>Select one</option>
              {settings?.departments.map((department) => (
                <option key={department} value={department}>
                  {department.replace(/_/g, " ")}
                </option>
              ))}
            </SelectInput>
          </MediumBlock>
          <MediumBlock>
            <SelectInput name="semester" label="Semester">
              <option>Select one</option>
              {settings?.semesters.map((semester) => (
                <option key={semester} value={semester}>
                  {semester.replace(/_/g, " ")}
                </option>
              ))}
            </SelectInput>
          </MediumBlock>
          <MediumBlock>
            <TextInput
              name="year_of_passout"
              type="number"
              label="Year of passout"
            />
          </MediumBlock>
          <MediumBlock>
            <TextInput name="register_no" type="text" label="Register No" />
          </MediumBlock>
          <MediumBlock>
            <TextInput name="institution" type="text" label="Institution" />
          </MediumBlock>
        </div>
        <div className="flex max-w-sm mb-4 ml-2">
          <FormButton>Update Profile</FormButton>
        </div>
      </Form>
    </Formik>
  );
}

export default StudentProfileEditForm;

function MediumBlock({
  children,
  block = false,
}: {
  children: React.ReactNode;
  block?: boolean;
}) {
  const comp = (
    <div className="w-full px-2 my-2 lg:w-1/2 2xl:w-1/3">{children}</div>
  );

  return block ? <div className="w-full">{comp}</div> : comp;
}
