import React from "react";
import { useFetchData } from "../../lib/adapters/fetcher";
import { GrTypeType } from "../../lib/constants/grievance";
import { toastError } from "../../lib/helpers/toastError";
import { GrievanceType } from "../../lib/types/db";
import GrievanceTable from "./GrievanceTable";

function AllGrievances({ type }: { type?: GrTypeType }) {
  const _typeString = type ? `?type=${type}` : "";
  const { data, error } = useFetchData<GrievanceType[] | null>(
    `/api/grievances/all${_typeString}`
  );
  toastError(error);
  if (!data)
    return (
      <div className="w-full h-20 my-6 bg-gray-300 rounded-lg dark:bg-gray-600 animate-pulse"></div>
    );
  return <GrievanceTable grievances={data} showNew={false} />;
}

export default AllGrievances;
