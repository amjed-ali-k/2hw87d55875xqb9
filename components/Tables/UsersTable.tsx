import React from "react";
import _d from "dayjs";
import {
  TableBody,
  TableContainer,
  Table,
  TableHeader,
  TableCell,
  TableRow,
  TableFooter,
  Pagination,
  Button,
  Badge,
} from "@windmill/react-ui";
import { useTable, useSortBy, usePagination } from "react-table";
import {
  TiArrowSortedDown,
  TiArrowSortedUp,
  TiArrowUnsorted,
} from "react-icons/ti";

import { FiEdit } from "react-icons/fi";
import { BiTrash } from "react-icons/bi";
import { UserWithProfile } from "../../lib/types/db";
export default UsersTable;

function UsersTable({ users }: { users?: UserWithProfile[] }) {
  const userData = users?.map((user) => ({
    fullname: user.name,
    type: user.type,
    registered: user.createdAt,
    department: user.department,
    key: user.key,
  }));

  const cols = React.useMemo(
    () => [
      {
        Header: "Full Name",
        accessor: "fullname", // accessor is the "key" in the data
      },
      {
        Header: "Type",
        accessor: "type",
      },
      {
        Header: "Registered On",
        accessor: "registered",
      },
      {
        Header: "Department",
        accessor: "department",
      },
      {
        Header: "Key",
        accessor: "key",
      },
    ],
    []
  ) as any;
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const data = React.useMemo(() => userData, [users]);

  const tableInstance = useTable(
    { columns: cols, data: data ? data : [] },
    useSortBy,
    usePagination
  );
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page,
    gotoPage,
    state: { pageSize },
  } = tableInstance;
  return (
    <TableContainer>
      <Table {...getTableProps()}>
        <TableHeader>
          {
            // Loop over the header rows
            headerGroups.map((headerGroup) => (
              // Apply the header row props
              <tr
                {...headerGroup.getHeaderGroupProps()}
                key={headerGroup.getHeaderGroupProps().key}
              >
                {
                  // Loop over the headers in each row
                  headerGroup.headers.map((column: any) => (
                    // Apply the header cell props
                    <TableCell
                      {...column.getHeaderProps(column.getSortByToggleProps())}
                      key={
                        column.getHeaderProps(column.getSortByToggleProps()).key
                      }
                    >
                      <div className="flex items-center">
                        {
                          // Render the header
                          column.render("Header")
                        }
                        <span>
                          {column.isSorted ? (
                            column.isSortedDesc ? (
                              <TiArrowSortedDown />
                            ) : (
                              <TiArrowSortedUp />
                            )
                          ) : (
                            <TiArrowUnsorted />
                          )}
                        </span>
                      </div>
                    </TableCell>
                  ))
                }
                <TableCell>Actions</TableCell>
              </tr>
            ))
          }
        </TableHeader>
        <TableBody {...getTableBodyProps()}>
          {
            // Loop over the table rows
            page.map((row) => {
              // Prepare the row for display
              prepareRow(row);
              const key = row.cells[4].value as string;
              const user = users?.filter((_user) => _user.key === key)[0];
              return (
                // Apply the row props
                <TableRow {...row.getRowProps()} key={row.getRowProps().key}>
                  <TableCell {...row.cells[0].getCellProps()}>
                    <p className="font-semibold">
                      {row.cells[0].render("Cell")}
                    </p>

                    <p className="text-xs">
                      {user?.type === "student"? `Passout: ${user.year_of_passout} | ${user.semester} Semester | Reg No. ${user.register_no}`: ""}
                   {user?.type === "staff"? `Designation: ${user.designation}`: ""}
                   {user?.type === "parent"? `Parent of: ${user.student_name} | ${user.semester} Semester`: ""}
                    </p>
                  </TableCell>
                  <TableCell {...row.cells[1].getCellProps()}>
                    <span className="text-sm">
                      <Badge className="mr-1">
                        {row.cells[1].render("Cell")}
                      </Badge>
                    </span>
                  </TableCell>
                  <TableCell {...row.cells[2].getCellProps()}>
                    <span className="text-sm">
                      {_d(new Date(row.cells[2].value)).format(
                        "D MMM YYYY h:mm A"
                      )}
                    </span>
                  </TableCell>
                  <TableCell {...row.cells[3].getCellProps()}>
                    <span className="text-sm">
                      {row.cells[3].render("Cell")}{" "}
                    </span>
                  </TableCell>
                  <TableCell {...row.cells[4].getCellProps()}>
                    <span className="text-sm">
                      {row.cells[4].value.slice(0, 10)}...
                    </span>
                  </TableCell>

                  <TableCell {...row.cells[4].getCellProps()}>
                    <Button
                      size="small"
                      className="mr-2"
                      icon={FiEdit}
                    ></Button>
                    <Button
                      size="small"
                      layout="outline"
                      icon={BiTrash}
                    ></Button>
                  </TableCell>
                </TableRow>
              );
            })
          }
        </TableBody>
      </Table>
      <TableFooter>
        <Pagination
          totalResults={users ? users.length : 0}
          resultsPerPage={pageSize}
          label="Table navigation"
          onChange={(e) => {
            gotoPage(e);
          }}
        />
      </TableFooter>
    </TableContainer>
  );
}
