import React from "react";
import UsersTable from "../../components/Tables/UsersTable";
import PageTitle from "../../components/Typography/PageTitle";
import Layout from "../../containers/Layout";
import { useFetchPermenantData } from "../../lib/adapters/fetcher";
import { UserWithProfile } from "../../lib/types/db";

function CustomPage() {
  const { data: users} = useFetchPermenantData<UserWithProfile[]>("/api/users/all");
  console.log(users)
  return (
    <Layout>
      <PageTitle>All Users</PageTitle>
      <div className="flex flex-wrap w-full mb-4 lg:flex-row-reverse">
        <div className="w-full ">
          <UsersTable users={users} />
        </div>
      </div>
    </Layout>
  );
}

export default CustomPage;
