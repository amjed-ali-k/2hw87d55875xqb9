import {
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Select,
} from "@windmill/react-ui";
import { useState } from "react";
import { FaPlusCircle, FaTimes } from "react-icons/fa";
import { RiFileUserLine } from "react-icons/ri";
import { useSWRConfig } from "swr";
import { FormCard, Masonary } from "../../components/Forms/FormCard";
import PageTitle from "../../components/Typography/PageTitle";
import Layout from "../../containers/Layout";
import {
  useFetchPermenantData,
} from "../../lib/adapters/fetcher";
import { Permissions, Roles } from "../../lib/constants/users";
import httpClient from "../../lib/helpers/httpClient";
import { PermissionsType, UserType } from "../../lib/types/db";

/* 
    This is the roles page.
    1. LIst users seperated by roles
    2. Permissions for each role
*/
function CustomPage() {
  return (
    <Layout>
      <PageTitle>Memberships and Roles</PageTitle>
      <div className="flex flex-wrap">
        <GrsCard
          role={Roles.STAFF_GREIVENCE_CELL_MEMBER}
          title="Staff Grievance Cell Members"
          description="Add or remove staff greivance cell members. Staff grievance cell members can see and respond to all grievances made by staffs. Usually staffs are only assigned to this role."
        />
        <GrsCard
          role={Roles.STUDENT_GREIVENCE_CELL_MEMBER}
          title="Student Grievance Cell Members"
          description="Add or remove student greivance cell members. Student grievance cell members can see and respond to all grievances made by students and parents. Usually staffs are only assigned to this role."
        />
        <GrsCard
          role={Roles.INTERNAL_COMPLAINTS_CELL_MEMBER}
          title="Internal Complaints Committee Members"
          description="Add or remove Internal Complaints members. Internal complaints cell members can see and respond to all internal complaints recieved in the institution."
        />
        <GrsCard
          role={Roles.ADMIN}
          title="Administrators"
          description="Add or remove system administrators. Administrators will have full access to the system. They can add, edit, delete and view all data."
        />
      </div>
      <PageTitle>Roles and Permissions</PageTitle>
      <Masonary large>
        <PermissionsContainer
          title="Internal Complaints Committee Members"
          description="Add or remove permissions for Internal Complaints members."
          role={Roles.INTERNAL_COMPLAINTS_CELL_MEMBER}
        />
        <PermissionsContainer
          title="Sudent Grievance Cell Members"
          description="Add or remove permissions for Internal Complaints members."
          role={Roles.STUDENT_GREIVENCE_CELL_MEMBER}
        />
        <PermissionsContainer
          title="Staff Grievance Cell Members"
          description="Add or remove permissions for Internal Complaints members."
          role={Roles.STAFF_GREIVENCE_CELL_MEMBER}
        />
        <PermissionsContainer
          title="Staffs"
          description="Add or remove permissions for Internal Complaints members."
          role={Roles.STAFF}
        />
        <PermissionsContainer
          title="Students"
          description="Add or remove permissions for Internal Complaints members."
          role={Roles.STUDENT}
        />
        <PermissionsContainer
          title="Parents"
          description="Add or remove permissions for Internal Complaints members."
          role={Roles.PARENT}
        />
      </Masonary>
    </Layout>
  );
}

export default CustomPage;

function UserCard({
  user,
  onRemove = () => {},
}: {
  user?: UserType;
  onRemove?: (userId?: string) => void;
}) {
  return (
    <div
      draggable
      className="relative inline-flex items-center p-2 m-1 text-sm border-2 rounded-md group dark:border-slate-600"
    >
      <div
        onClick={() => onRemove(user?.key)}
        className="absolute p-1 text-sm text-white transition-opacity duration-500 bg-red-700 rounded-full opacity-0 cursor-pointer focus:bg-red-300 group-hover:opacity-100 hover:bg-red-400 top-1 right-1"
      >
        <FaTimes size={10} />
      </div>
      <div className="p-2 mr-2 rounded-full bg-slate-200 dark:bg-slate-600">
        <RiFileUserLine size={30} />
      </div>
      <div className="flex-grow-1">
        <p className="font-semibold">{user?.name}</p>
        <p className="capitalize">
          {user && user.type === "staff" && user?.designation}{" "}
          {user?.department}
        </p>
      </div>
    </div>
  );
}

function PermissionsCard({
  name,
  onRemove,
}: {
  name: string;
  onRemove: (permission: string) => void;
}) {
  return (
    <div
      draggable
      onClick={() => onRemove(name)}
      className="inline-flex items-center justify-center px-3 py-1 m-1 space-x-2 text-sm font-semibold capitalize border-2 rounded-md hover:border-opacity-60 dark:border-slate-600"
    >
      <p>{name.replace(/-/g, ' ')}</p>
      <span className="cursor-pointer hover:opacity-70">
        <FaTimes size={15} />
      </span>
    </div>
  );
}

function GrsCard({
  role,
  title,
  description,
}: {
  role: Roles;
  title: string;
  description: string;
}) {
  const [showModal, setshowModal] = useState(false);
  const { data } = useFetchPermenantData<UserType[]>(
    `/api/admin/users/roles?role=${role}`
  );
  const { mutate } = useSWRConfig();
  const handleRemove = async (userId: string | undefined) => {
    await httpClient.post(`/api/admin/users/remove-role`, {
      userId,
      role: role,
    });
    mutate(`/api/admin/users/roles?role=${role}`);
  };

  const handleNew = async (user?: UserType) => {
    setshowModal(false);
    if (!user) return null;
    await httpClient.post(`/api/admin/users/add-role`, {
      role: role,
      userId: user.id,
    });
    mutate(`/api/admin/users/roles?role=${role}`);
  };
  return (
    <div className="w-full xl:w-1/2">
      <FormCard title={title} description={description}>
        <div className="flex flex-wrap">
          {data &&
            data.map((user) => (
              <UserCard user={user} key={user.id} onRemove={handleRemove} />
            ))}
          <div className="w-full">
            <div
              onClick={() => setshowModal(true)}
              className="inline-flex items-center justify-center px-3 m-1 space-x-2 text-sm font-semibold border-2 rounded-md cursor-pointer hover:border-opacity-60 h-14 dark:border-slate-600"
            >
              <p>Add new</p>
              <FaPlusCircle size={30} />
            </div>
          </div>
        </div>
      </FormCard>
      <AddUserModal
        onClose={() => setshowModal(false)}
        onSubmit={handleNew}
        open={showModal}
      />
    </div>
  );
}

function AddUserModal({
  onClose,
  onSubmit,
  open,
}: {
  onClose: () => void;
  onSubmit: (user?: UserType) => void;
  open: boolean;
}) {
  const { data } = useFetchPermenantData<UserType[]>("/api/users/all/");
  const user = data;
  const [selectedUser, setSelectedUser] = useState<UserType>();

  return (
    <Modal isOpen={open} onClose={onClose}>
      <ModalHeader>Select User</ModalHeader>
      <ModalBody>
        <Select
          css=""
          name="user-select"
          onChange={(e) =>
            setSelectedUser(user && user[parseInt(e.target.value)])
          }
        >
          <option>Select</option>
          {user &&
            user.map((u, i) => (
              <option key={u.id} value={i}>
                {u.name}
              </option>
            ))}
        </Select>
      </ModalBody>
      <ModalFooter>
        <div className="hidden sm:block">
          <Button layout="outline" onClick={onClose}>
            Cancel
          </Button>
        </div>
        <div className="hidden sm:block">
          <Button
            onClick={() => {
              onSubmit(selectedUser);
            }}
          >
            Accept
          </Button>
        </div>
        <div className="block w-full sm:hidden">
          <Button block size="large" layout="outline" onClick={onClose}>
            Cancel
          </Button>
        </div>
        <div className="block w-full sm:hidden">
          <Button
            block
            size="large"
            onClick={() => {
              onSubmit(selectedUser);
            }}
          >
            Accept
          </Button>
        </div>
      </ModalFooter>
    </Modal>
  );
}

function PermissionsContainer({
  title,
  description,
  role,
}: {
  title: string;
  description: string;
  role: Roles;
}) {
  const { mutate } = useSWRConfig();
  const [showModal, setshowModal] = useState(false);
  const { data } = useFetchPermenantData<PermissionsType>(
    `/api/admin/permissions/role?role=${role}`
  );
  const handleRemove = async (permission: string | undefined) => {
    await httpClient.post(`/api/admin/permissions/remove-role`, {
      permission,
      role: role,
    });
    mutate(`/api/admin/permissions/role?role=${role}`);
  };

  const handleNew = async (permission?: string) => {
    setshowModal(false);
    console.log(permission)
    if (!permission) return null;
    await httpClient.post(`/api/admin/permissions/add-role`, {
      role: role,
      permission: permission,
    });
    mutate(`/api/admin/permissions/role?role=${role}`);
  };

  return (
    <FormCard title={title} description={description}>
      <div className="flex flex-wrap">
        {data?.permissions.map((p) => (
          <PermissionsCard key={p} name={p} onRemove={handleRemove} />
        ))}

        <div
          onClick={() => setshowModal(true)}
          className="inline-flex items-center justify-center px-3 py-1 m-1 space-x-2 text-sm font-semibold border-2 rounded-md cursor-pointer hover:border-opacity-60 dark:border-slate-600"
        >
          <p>Add new</p>
          <FaPlusCircle size={15} />
        </div>
      </div>
      <AddPermissionsModal
        onClose={() => setshowModal(false)}
        onSubmit={handleNew}
        open={showModal}
      />
    </FormCard>
  );
}

function AddPermissionsModal({
  onClose,
  onSubmit,
  open,
}: {
  onClose: () => void;
  onSubmit: (permission?: string) => void;
  open: boolean;
}) {
  const permissions = Object.values(Permissions);
  const [selectedPermission, setSelectedPermission] = useState<string>();

  return (
    <Modal isOpen={open} onClose={onClose}>
      <ModalHeader>Select Permission</ModalHeader>
      <ModalBody>
        <Select
          css=""
          name="user-select"
          onChange={(e) => setSelectedPermission(e.target.value)}
        >
          <option>Select</option>
          {permissions.map((u, i) => (
            <option className="capitalize" key={u} value={u}>
              {u.replace(/-/g, ' ')}
            </option>
          ))}
        </Select>
      </ModalBody>
      <ModalFooter>
        <div className="hidden sm:block">
          <Button layout="outline" onClick={onClose}>
            Cancel
          </Button>
        </div>
        <div className="hidden sm:block">
          <Button
            onClick={() => {
              onSubmit(selectedPermission);
            }}
          >
            Accept
          </Button>
        </div>
        <div className="block w-full sm:hidden">
          <Button block size="large" layout="outline" onClick={onClose}>
            Cancel
          </Button>
        </div>
        <div className="block w-full sm:hidden">
          <Button
            block
            size="large"
            onClick={() => {
              onSubmit(selectedPermission);
            }}
          >
            Accept
          </Button>
        </div>
      </ModalFooter>
    </Modal>
  );
}
