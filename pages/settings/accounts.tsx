import React from "react";
import toast from "react-hot-toast";
import { useSWRConfig } from "swr";
import CheckBox from "../../components/Forms/CheckBox";
import { Half, Masonary, FormikCard } from "../../components/Forms/FormCard";
import TextAreaInput from "../../components/Forms/TextAreaInput";
import TextInput from "../../components/Forms/TextInput";
import PageTitle from "../../components/Typography/PageTitle";
import Layout from "../../containers/Layout";
import { useFetchPermenantData } from "../../lib/adapters/fetcher";
import {
  AuthSettings,
  EmailSettings,
  OAuthSettings,
  SettingsType,
} from "../../lib/constants/settings";
import httpClient from "../../lib/helpers/httpClient";
import { toastError } from "../../lib/helpers/toastError";
/* 
    This is the acounts page.
    1. Login and Registration settings
    2. Password Complexity
    3. Blacklist Domains
    4. Whitelist Domains
    5. Email Settings
    6. OAuth Accounts

*/
function CustomPage() {
  return (
    <Layout>
      <PageTitle>Account Settings</PageTitle>
      <Masonary>
        <LoginCard />
        <PasswordComplexity />
        <BlackListed />
        <WhiteListed />
        <ProfileSettings />
        <EmailSettingsCard />
        <OAuthCard />
      </Masonary>
    </Layout>
  );
}

export default CustomPage;

function OAuthForm({ provider }: { provider: string }) {
  return (
    <>
      <CheckBox name={`${provider}`}>Enable {provider} authentication</CheckBox>

      <>
        <TextInput
          label={`${provider} Client Id`}
          name={`${provider}_ClientId`}
          type="text"
          placeholder={`${provider} Client ID`}
        />
        <TextInput
          label={`${provider} Client Secret`}
          name={`${provider}_ClientSecret`}
          type="password"
          placeholder={`${provider} Client Secret`}
        />
      </>
    </>
  );
}

function LoginCard() {
  const { data: settings } = useFetchPermenantData<AuthSettings>(
    `/api/admin/settings/get?type=${SettingsType.Auth}`
  );
  const { mutate } = useSWRConfig();
  return (
    <FormikCard
      title="Login and Registration settings"
      description="Here you can change the settings for the login and registration."
      initialValues={settings}
      onSubmit={async (values) => {
        try {
          await httpClient.post(`/api/admin/settings/update`, {
            settings: values,
            type: SettingsType.Auth,
          });
          toast.success("Updated successfully");
          mutate(`/api/admin/settings/get?type=${SettingsType.Auth}`);
        } catch (error) {
          toastError(error);
        }
      }}
    >
      <CheckBox name="allowRegistration">
        Allow registration to the system
      </CheckBox>
      <CheckBox name="allowResetPassword">Allow forgot password</CheckBox>
      <CheckBox name="enableRecaptcha">
        Allow recaptcha for authentication
      </CheckBox>
      <TextInput
        label={"Google Captcha"}
        name="recaptchaSiteKey"
        placeholder="Recaptcha sitekey"
        disabled
      />
    </FormikCard>
  );
}

function PasswordComplexity() {
  const { data: settings } = useFetchPermenantData<AuthSettings>(
    `/api/admin/settings/get?type=${SettingsType.Auth}`
  );
  const { mutate } = useSWRConfig();

  return (
    <FormikCard
      title="Password Complexity"
      description="Password Complexity default of all new users in the system. This
        settings will not affect existing users until they change their
        password."
      initialValues={settings}
      onSubmit={async (values) => {
        try {
          await httpClient.post(`/api/admin/settings/update`, {
            settings: values,
            type: SettingsType.Auth,
          });
          toast.success("Updated successfully");
          mutate(`/api/admin/settings/get?type=${SettingsType.Auth}`);
        } catch (error) {
          toastError(error);
        }
      }}
    >
      <CheckBox name="passwordUppercaseLetter">
        Passwords require Uppercase charecters
      </CheckBox>
      <CheckBox name="passwordLowercaseLetter">
        Passwords require Lowercase charecters
      </CheckBox>
      <CheckBox name="passwordNumber">Passwords require Numbers</CheckBox>
      <CheckBox name="passwordSpecialCharacter">
        Passwords require Special charecters
      </CheckBox>
      <div className="flex">
        <Half>
          <TextInput
            label={"Minimum Length"}
            name="passwordMinLength"
            type="number"
            placeholder="5"
          />
        </Half>
        <Half>
          <TextInput
            label={"Maximum Length"}
            name="passwordMaxLength"
            type="number"
            placeholder="15"
          />
        </Half>
      </div>
    </FormikCard>
  );
}

function BlackListed() {
  const { data: settings } = useFetchPermenantData<AuthSettings>(
    `/api/admin/settings/get?type=${SettingsType.Auth}`
  );
  const { mutate } = useSWRConfig();

  return (
    <FormikCard
      title="Blacklisted Domains"
      description="You can blacklist domains that are not allowed to register. Blacklisted Domains will not apply for OAuth Login"
      initialValues={{
        blacklistedDomains: settings?.blacklistedDomains?.join(";"),
        enableBL: settings?.blacklistedDomains?.length !== 0
      }}
      onSubmit={async (values) => {
        let domains = values.blacklistedDomains === "" ? []: values.blacklistedDomains.split(";")
        if(!values.enableBL) domains = []
        try {
          await httpClient.post(`/api/admin/settings/update`, {
            settings: {
              blacklistedDomains: domains,
            },
            type: SettingsType.Auth,
          });
          toast.success("Updated successfully");
          mutate(`/api/admin/settings/get?type=${SettingsType.Auth}`);
        } catch (error) {
          toastError(error);
        }
      }}
    >
      <CheckBox name="enableBL">Enable Domain blacklist</CheckBox>
      <TextAreaInput
        label="Blacklist (Enter domain names seperated by semicolon)"
        name="blacklistedDomains"
        rows={4}
        placeholder="google.com; yahoo.com; wikipedia.com;"
      ></TextAreaInput>
    </FormikCard>
  );
}

function WhiteListed() {
  const { data: settings } = useFetchPermenantData<AuthSettings>(
    `/api/admin/settings/get?type=${SettingsType.Auth}`
  );
  const { mutate } = useSWRConfig();
  console.log("Settings",settings)
  return (
    <FormikCard
      title="Whitelisted Domains"
      description="You can whitelist domains that are only allowed to register. Blacklisted domains will be ignored. Whitelisted Domains will not apply for OAuth Login"
      initialValues={{
        whiteListedDomains: settings?.whiteListedDomains?.join(";"),
        enableWL: settings?.whiteListedDomains?.length !== 0
      }}
      onSubmit={async (values) => {
        let domains = values.whiteListedDomains === "" ? []: values.whiteListedDomains.split(";")
        if(!values.enableWL) domains = []
        try {
          await httpClient.post(`/api/admin/settings/update`, {
            settings: {
              whiteListedDomains: domains,
            },
            type: SettingsType.Auth,
          });
          toast.success("Updated successfully");
          mutate(`/api/admin/settings/get?type=${SettingsType.Auth}`);
        } catch (error) {
          toastError(error);
        }
      }}
    >
      <CheckBox name="enableWL">Enable Domain Whitelist</CheckBox>
      <TextAreaInput
        label="Whitelist (Enter domain names seperated by semicolon)"
        name="whiteListedDomains"
        rows={4}
        placeholder="google.com; yahoo.com; wikipedia.com;"
      ></TextAreaInput>
    </FormikCard>
  );
}

function EmailSettingsCard() {
  const { data: settings } = useFetchPermenantData<EmailSettings>(
    `/api/admin/settings/get?type=${SettingsType.Email}`
  );
  const { mutate } = useSWRConfig();
  return (
    <FormikCard
      title="Email Settings"
      description="Login and Registration email notifications. This settings only works if emails are enabled and configured properly"
      initialValues={settings}
      onSubmit={async (values) => {
        try {
          await httpClient.post(`/api/admin/settings/update`, {
            settings: values,
            type: SettingsType.Email,
          });
          toast.success("Updated successfully");
          mutate(`/api/admin/settings/get?type=${SettingsType.Email}`);
        } catch (error) {
          toastError(error);
        }
      }}
    >
      <CheckBox name="registrationEnabled">
        Send emails to users when they register
      </CheckBox>
      <CheckBox name="loginEnabled">
        Send emails to users when they login
      </CheckBox>
      <TextInput
        label={"Registration Email Subject"}
        name="registrationSubject"
        type="text"
        placeholder="You are successfully registered on GRS system"
      />
      <TextInput
        label={"Login Email Subject"}
        name="registrationSubject"
        type="number"
        placeholder="New login to GRS system"
      />
    </FormikCard>
  );
}

function OAuthCard() {
  const { data } = useFetchPermenantData<OAuthSettings>(
    `/api/admin/settings/get?type=${SettingsType.OAuth}`
  );

  const settings = {
    allowPasswordLogin: data?.allowPasswordLogin,
    google: data?.google.enabled,
    google_ClientId: data?.google.clientId,
    google_ClientSecret: data?.google.clientSecret,
    facebook: data?.facebook.enabled,
    facebook_ClientId: data?.facebook.clientId,
    facebook_ClientSecret: data?.facebook.clientSecret,
    github: data?.github.enabled,
    github_ClientId: data?.github.clientId,
    github_ClientSecret: data?.github.clientSecret,
    twitter: data?.twitter.enabled,
    twitter_ClientId: data?.twitter.clientId,
    twitter_ClientSecret: data?.twitter.clientSecret,
    linkedin: data?.linkedin.enabled,
    linkedin_ClientId: data?.linkedin.clientId,
    linkedin_ClientSecret: data?.linkedin.clientSecret,
  };
  console.log(settings);
  const { mutate } = useSWRConfig();

  return (
    <FormikCard
      title="OAuth login"
      description="OAuth (Open Authorization) is an open standard for access delegation, commonly used as a way for Internet users to grant websites or applications access to their information on other websites but without giving them the passwords. This mechanism is used by companies such as Amazon, Google, Facebook, Microsoft and Twitter to permit the users to share information about their accounts with third-party applications or websites."
      initialValues={settings}
      onSubmit={async (values) => {
        try {
          await httpClient.post(`/api/admin/settings/update`, {
            settings: {
              allowPasswordLogin: values.allowPasswordLogin,
              google: {
                enabled: values.google,
                clientId: values.google_ClientId,
                clientSecret: values.google_ClientSecret,
              },
              facebook: {
                enabled: values.facebook,
                clientId: values.facebook_ClientId,
                clientSecret: values.facebook_ClientSecret,
              },
              twitter: {
                enabled: values.twitter,
                clientId: values.twitter_ClientId,
                clientSecret: values.twitter_ClientSecret,
              },
              github: {
                enabled: values.github,
                clientId: values.github_ClientId,
                clientSecret: values.github_ClientSecret,
              },
              linkedin: {
                enabled: values.linkedin,
                clientId: values.linkedin_ClientId,
                clientSecret: values.linkedin_ClientSecret,
              },
            },
            type: SettingsType.OAuth,
          });
          toast.success("Updated successfully");
          mutate(`/api/admin/settings/get?type=${SettingsType.OAuth}`);
        } catch (error) {
          toastError(error);
        }
      }}
    >
      <CheckBox name="allowPasswordLogin">Enable Password login</CheckBox>
      <OAuthForm provider="google" />
      <OAuthForm provider="facebook" />
      <OAuthForm provider="twitter" />
      <OAuthForm provider="github" />
      <OAuthForm provider="linkedin" />
    </FormikCard>
  );
}

function ProfileSettings() {
  const { data: settings } = useFetchPermenantData<AuthSettings>(
    `/api/admin/settings/get?type=${SettingsType.Auth}`
  );
  const { mutate } = useSWRConfig();
  return (
    <FormikCard
      title="Profile Settings"
      description="Here you can add and remove profile fields"
      initialValues={{
        semesters: settings?.semesters?.join(";\n").replace(/_/g, " "),
        designations: settings?.designations?.join(";\n").replace(/_/g, " "),
        departments: settings?.departments?.join(";\n").replace(/_/g, " "),
      }}
      onSubmit={async (values) => {
        try {
          await httpClient.post(`/api/admin/settings/update`, {
            settings: {
              semesters: values.semesters.replace(/\s/g, '_').replace(/\n/g, "").split(";"),
              designations: values.designations.replace(/\s/g, '_').replace(/\n/g, "").split(";"),
              departments: values.departments.replace(/\s/g, '_').replace(/\n/g, "").split(";"),
            },
            type: SettingsType.Auth,
          });
          toast.success("Updated successfully");
          mutate(`/api/admin/settings/get?type=${SettingsType.Auth}`);
        } catch (error) {
          toastError(error);
        }
      }}
    >
      <TextAreaInput
        label="Semesters (Enter semester names seperated by semicolon)"
        name="semesters"
        rows={4}
        placeholder="First; Second; Third; Forth; Fifth; Sixth"
      />
      <TextAreaInput
        label="Staff Designations (Enter names seperated by semicolon)"
        name="designations"
        rows={4}
        placeholder="Principal; Head of Department; Assistant Professor"
      />
      <TextAreaInput
        label="Departments or Sections (Enter names seperated by semicolon)"
        name="departments"
        rows={4}
        placeholder="Office Section; Department of Mathematics; Department of Physics"
      />
    </FormikCard>
  );
}
