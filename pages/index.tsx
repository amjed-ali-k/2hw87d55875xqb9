import { Button } from "@windmill/react-ui";
import type { NextPage } from "next";
import Head from "next/head";
import Image from "next/image";
import Link from "next/link";
import { IconType } from "react-icons";
import { AiFillGoogleCircle, AiOutlineFileSearch } from "react-icons/ai";
import { BiEditAlt } from "react-icons/bi";
import { BsBellFill } from "react-icons/bs";
import { CgTimer } from "react-icons/cg";
import { GiBoatPropeller, GiFastArrow } from "react-icons/gi";
import { HiSearchCircle } from "react-icons/hi";
import { MdOutlineDocumentScanner } from "react-icons/md";
import { useFetchPermenantData } from "../lib/adapters/fetcher";
import { FrontPageSettings } from "../lib/constants/settings";
declare global {
  interface Window {
    dataLayer: any;
  }
}

const Home: NextPage = () => {
  const { data: settings } = useFetchPermenantData<FrontPageSettings>(
    "/api/settings/frontPage"
  );

  const injectGA = () => {
    if (typeof window == "undefined") return;
    if (settings && !settings.analyticsEnabled) return null;
    window.dataLayer = window.dataLayer || [];
    function gtag(...args: any[]) {
      window.dataLayer.push(arguments);
    }
    gtag("js", new Date());
    gtag("config", settings?.analyticsTrackingId);
    return null;
  };

  return (
    <>
      <Head>
        <title>Pixie GRS - Advanced Grievance Redressal System</title>
      </Head>
      {settings?.analyticsEnabled && (
        <>
          <script
            async
            src={
              "https://www.googletagmanager.com/gtag/js?id=" +
              settings.analyticsTrackingId
            }
          />
          <script>{injectGA()}</script>
        </>
      )}
      <section>
        <div className="flex flex-col min-h-screen">
          <div className="flex-grow">
            <header className="relative bg-gray-700">
              <div>
                <Image
                  src="/images/cover.jpg"
                  alt="Cover"
                  layout="fill"
                  objectFit="cover"
                />
              </div>
              <div className="absolute inset-0 w-full h-full overflow-hidden opacity-50 bg-gradient-to-r from-black to-gray-700 bg-blend-darken" />

              <div className="container relative flex px-3 py-24 mx-auto text-white md:px-0 md:pt-48 md:pb-56">
                <div className="w-full lg:w-2/3">
                  <div className="items-center hidden p-2 leading-none bg-teal-800 rounded-full bg-opacity-60 md:flex">
                    <span className="flex px-2 py-1 mr-3 text-xs font-bold tracking-wider uppercase bg-orange-600 rounded-full">
                      Update
                    </span>
                    <span className="flex-auto mr-2 text-sm text-left md:text-base">
                      Now fully integrated with Telegram and Whatsapp
                    </span>
                  </div>

                  <h1 className="mt-4">Pixie Grievance Redressal System</h1>
                  <p className="w-full mt-1 text-xl font-extrabold lg:w-4/5 md:text-3xl">
                    Powerful Grievance Redressal System (GRS) for instutitions
                  </p>
                  <div className="mt-12">
                    <div className="w-24 h-24 filter grayscale invert md:w-[150px] md:h-[150px]">
                      <Image
                        src="/logo.png"
                        alt="Pixie GRS"
                        width="150"
                        height="150"
                      />
                    </div>
                    <div className="mt-2 space-x-4 text-base text-white button sm:text-xl focus:outline-none">
                      <span>
                        Government Polytechnic College, Perinthalmanna
                      </span>
                    </div>
                    <p className="text-xs md:mt-2 md:text-sm">
                      Student Grievance Cell, Internal Complaints cell
                    </p>
                  </div>
                  <div className="mt-4">
                    <Link href="/login">
                      <a>
                        <Button
                          type="button"
                          layout="outline"
                          className="mx-2 px-14"
                        >
                          Login
                        </Button>
                      </a>
                    </Link>
                    <Link href="/create-account">
                      <a>
                        <Button
                          type="button"
                          layout="outline"
                          className="mx-2 px-14"
                        >
                          Register
                        </Button>
                      </a>
                    </Link>
                  </div>
                </div>
              </div>
            </header>
          </div>
        </div>
      </section>
      <GrievanceMembersSections
        title="Student Grievance Cell Members"
        members={grsMembers}
      />
      <GrievanceMembersSections
        title="Staff Grievance Cell Members"
        members={grsMembers}
      />
      <GrievanceMembersSections
        title="Internal Complaints Committee Members"
        members={grsMembers}
      />
      <section className="text-white bg-purple-900">
        <div className="container px-3 py-24 mx-auto">
          <h3 className="my-5 text-4xl font-bold text-center">Features</h3>
          <div className="grid grid-cols-2 gap-4 md:grid-cols-4">
            <Feature
              title="Fast"
              icon={GiFastArrow}
              des="Pixie grs is a high performance application made using latest web technologies"
            />
            <Feature
              title="As per AICTE Norms"
              icon={MdOutlineDocumentScanner}
              des="Pixie grs is a high performance application made using latest web technologies"
            />
            <Feature
              title="Customizable"
              icon={BiEditAlt}
              des="Pixie grs is a high performance application made using latest web technologies"
            />
            <Feature
              title="Direct Notifications"
              icon={BsBellFill}
              des="Pixie grs is a high performance application made using latest web technologies"
            />
            <Feature
              title="Social Login"
              icon={AiFillGoogleCircle}
              des="Pixie grs is a high performance application made using latest web technologies"
            />
            <Feature
              title="Customizable Reports"
              icon={AiOutlineFileSearch}
              des="Pixie grs is a high performance application made using latest web technologies"
            />
            <Feature
              title="SEO Optimized"
              icon={HiSearchCircle}
              des="Pixie grs is a high performance application made using latest web technologies"
            />
            <Feature
              title="24/7 Available"
              icon={CgTimer}
              des="Pixie grs is a high performance application made using latest web technologies"
            />
          </div>
        </div>
      </section>
      <footer className="text-white bg-blue-900">
        <div className="container flex justify-center mx-auto pt-7">
          <div className="flex flex-col items-center space-x-1 md:flex-row">
            <p className="px-3 border-blue-400 md:border-r-2">Privacy Policy</p>
            <p className="px-3 border-blue-400 md:border-r-2">Cookie Policy</p>
            <p className="px-3 border-blue-400 md:border-r-2">
              Terms and Conditions
            </p>
            <p className="px-3 border-blue-400 md:border-r-2">Register</p>
            <p className="px-3 border-blue-400 md:border-r-2">Login</p>
            <p className="px-3">Institution Website</p>
          </div>
        </div>
        <div className="container items-end px-3 mx-auto lg:flex lg:flex-row-reverse lg:justify-between">
          <div draggable className="py-3 text-xl font-bold text-center">
            <div className="flex justify-center">
              <GiBoatPropeller size={70} />
            </div>
            <p>Pixie GRS</p>
            <p className="text-xs">By CodeBolt Labs</p>
          </div>
          <div>
            <p className="py-4 text-sm text-center">
              Copyright © 2022 Codebolt labs | All rights reserved
            </p>
          </div>
        </div>
      </footer>
    </>
  );
};

export default Home;

function Feature({
  title,
  icon,
  des,
}: {
  title: string;
  icon: IconType;
  des: string;
}) {
  const Icon = icon;
  return (
    <div className="text-center ">
      <div className="flex justify-center">
        <Icon size={70} />
      </div>
      <h5 className="text-xl font-bold">{title}</h5>
      <p>{des}</p>
    </div>
  );
}

const grsMembers = [
  { name: "Amjed Ali K", designation: "Tradesman, Electronics" },
  { name: "Amjed Ali K", designation: "Tradesman, Electronics" },
  { name: "Amjed Ali K", designation: "Tradesman, Electronics" },
  { name: "Amjed Ali K", designation: "Tradesman, Electronics" },
];

function GrievanceMembersSections({
  title,
  members,
}: {
  title: string;
  members: { name: string; designation: string }[];
}) {
  return (
    <section>
      <div className="container mx-auto mb-20">
        <h3 className="my-5 text-4xl font-bold text-center">{title}</h3>
        <div className="flex flex-col flex-wrap justify-center md:flex-row">
          {members.map((m) => (
            <MemberCard key={m.name} member={m} />
          ))}
        </div>
      </div>
    </section>
  );
}

function MemberCard({
  member,
}: {
  member: { name: string; designation: string };
}) {
  return (
    <div className="flex m-2 overflow-hidden border-2 rounded-lg">
      <div className="relative h-full md:w-1/2">
        <div className="absolute inset-0 w-full h-full bg-indigo-800" />
        <div className="flex items-center justify-center p-3 filter grayscale invert">
          <Image src="/logo.png" alt="Pixie GRS" width="70" height="70" />
        </div>
      </div>
      <div className="flex-grow p-3">
        <div className="text-xl font-medium">{member.name}</div>
        <div className="text-sm">{member.designation}</div>
      </div>
    </div>
  );
}

{
  /* Global site tag (gtag.js) - Google Analytics */
}
