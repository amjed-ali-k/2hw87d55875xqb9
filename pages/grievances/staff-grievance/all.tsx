import React from "react";
import AlertBanner from "../../../components/Cards/AlertBanner";
import AllGrievances from "../../../components/Tables/AllGrievances";
import PageTitle from "../../../components/Typography/PageTitle";
import Layout from "../../../containers/Layout";
import { GrTypeType } from "../../../lib/constants/grievance";

function CustomPage() {
  return (
    <Layout>
      <PageTitle>All Staff Grievances</PageTitle>
      <AlertBanner>
        <p>
          <span className="pr-2 font-bold"> All Staff Grievances! </span>
          <span>In this page you can view all Staff Grievances posted.</span>
        </p>
      </AlertBanner>
      <div className="flex flex-wrap w-full mb-4 lg:flex-row-reverse">
        <div className="w-full ">
          <AllGrievances type={GrTypeType.STAFF} />
        </div>
      </div>
    </Layout>
  );
}

export default CustomPage;
