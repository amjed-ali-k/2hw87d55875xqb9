import React from "react";
import AlertBanner from "../../components/Cards/AlertBanner";
import MyGrievances from "../../components/Tables/MyGrievances";
import PageTitle from "../../components/Typography/PageTitle";
import Layout from "../../containers/Layout";

function CustomPage() {
  return (
    <Layout>
      <PageTitle>My Grievances</PageTitle>
      <AlertBanner>
        <p>
          <span className="pr-2 font-bold"> All Grievances! </span>
          <span>In this page you can view all grievances posted by you.</span>
        </p>
      </AlertBanner>
      <div className="flex flex-wrap w-full mb-4 lg:flex-row-reverse">
        <div className="w-full ">
          <MyGrievances />
        </div>
      </div>
    </Layout>
  );
}

export default CustomPage;
