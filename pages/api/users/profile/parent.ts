import * as yup from "yup";

import { ParentUserType } from "../../../../lib/types/db";
import validatedPost from "../../../../lib/helpers/validatedPost";
import {
  createNewProfile,
  getProfileByEmail,
  updateProfile,
} from "../../../../lib/db/profile";
import { parentProfileSchema } from "../../../../lib/schemas/profile";
import { getEmail, validatePermissions } from "../../../../lib/helpers/validateRoles";
import { addRoleToUser } from "../../../../lib/db/roles";
import { APIError, Permissions } from "../../../../lib/constants/users";
import { getUserByEmail } from "../../../../lib/db/auth";

const userValidationSchema: yup.SchemaOf<{}> = yup
  .object()
  .shape(parentProfileSchema)
  .noUnknown();

export default validatedPost(userValidationSchema, async (data, req) => {
  const email = await getEmail(req)
  if(!email) throw new Error(APIError.UNAUTHORIZED)
  const profile = { ...data } as unknown as ParentUserType;
  if ((profile.email) && !validatePermissions(email, [Permissions.UPDATE_USER])) throw new Error(APIError.UNAUTHORIZED)
  const existingProfile = await getProfileByEmail(profile.email ? profile.email : email);
  if (existingProfile) {
    await updateProfile({...profile, email: profile.email}, existingProfile.key);
    addRoleToUser(existingProfile, ["parent"]);
  } else {
    const user = await getUserByEmail(profile.email ? profile.email : email)
    if (!user) throw new Error(APIError.UNAUTHORIZED)
    const newEmail = user.email? user.email : email
    const pro = await createNewProfile({...profile, key: user.id, email: newEmail });
    pro && addRoleToUser(pro, ["parent"]);
  }
});
