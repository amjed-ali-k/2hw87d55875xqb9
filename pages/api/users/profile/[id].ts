import { Permissions } from "../../../../lib/constants/users";
import { getProfileByEmail } from "../../../../lib/db/profile";
import backendGet from "../../../../lib/helpers/backendGet";
import {
  getEmail,
  validatePermissions,
} from "../../../../lib/helpers/validateRoles";

export default backendGet(async (req) => {
  const email = await getEmail(req);
  if (!email) throw new Error("Not authorized");
  const { id } = req.query;
  const profile = await getProfileByEmail(Array.isArray(id) ? id[0] : id);
  if (!profile) throw new Error("Profile not found")
  if (
    profile.email === email ||
    ((await validatePermissions(email, [Permissions.VIEW_STAFF_USER])) &&
      profile.type === "staff") ||
    ((await validatePermissions(email, [Permissions.VIEW_STUDENT_USER])) &&
      profile.type === "student") ||
    ((await validatePermissions(email, [Permissions.VIEW_PARENT_USER])) &&
      profile.type === "parent")
  ) {
    return { ...profile, password: undefined };
  }
  throw new Error("Not authorized");
});
