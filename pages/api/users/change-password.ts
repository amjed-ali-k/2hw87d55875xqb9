import * as Yup from "yup";
import { compare, hash } from "bcryptjs";

import validatedPost from "../../../lib/helpers/validatedPost";
import { changePassword, getProfileByEmail } from "../../../lib/db/profile";
import { getEmail, validateRole } from "../../../lib/helpers/validateRoles";
import { Roles } from "../../../lib/constants/users";

const userValidationSchema: Yup.SchemaOf<{}> = Yup.object()
  .shape({
    current_password: Yup.string(),
    password: Yup.string().min(5).required("Required"),
    email: Yup.string().email(),
    repeat_password: Yup.string().oneOf(
      [Yup.ref("password"), null],
      "Passwords must match"
    ),
  })
  .noUnknown();

export default validatedPost(userValidationSchema, async (data, req) => {
  const email = await getEmail(req);
  if (!email) throw new Error("You are not allowed for this operation");
  const _data = { ...data } as {
    email: string;
    password: string;
    current_password?: string;
  };
  if (_data.current_password === "") delete _data["current_password"];
  const profile = await getProfileByEmail(_data.email ? _data.email : email);
  if (!profile) throw new Error("Not completed profile");
  const isAdmin = await validateRole([Roles.ADMIN], req, email);
  if (profile.email !== email && !isAdmin)
    throw new Error("You are not allowed for this operation"); // Permission check
  if (profile.password && !_data.current_password && !isAdmin)
    throw new Error("Current password not matches"); // Return
  if (_data.current_password && profile.password) {
    const isValid = await compare(_data.current_password, profile.password);
    if (!isValid) throw new Error("Current password not matches"); // Return
  }
  const hashed = await hash(_data.password, 10);
  return changePassword(profile, hashed);
});
