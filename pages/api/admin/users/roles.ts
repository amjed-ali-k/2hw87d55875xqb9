import { getUsersByRole } from "../../../../lib/db/roles";
import backendGet from "../../../../lib/helpers/backendGet";
import {
  getEmail,
  validateRole,
} from "../../../../lib/helpers/validateRoles";

export default backendGet(async (req) => {
  const { role } = req.query;
  const email = await getEmail(req);
  if (!email) throw new Error("Unauthorized");
  if(!await validateRole(['admin'], req, email)) throw new Error("Unauthorized");
  return getUsersByRole(role.toString())
});
