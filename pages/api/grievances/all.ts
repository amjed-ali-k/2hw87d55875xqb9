import { GrTypeType } from "../../../lib/constants/grievance";
import { Permissions } from "../../../lib/constants/users";
import { getAllGrievances } from "../../../lib/db/grievances";
import backendGet from "../../../lib/helpers/backendGet";
import {
  getEmail,
  validatePermissions,
} from "../../../lib/helpers/validateRoles";

export default backendGet(async (req) => {
  const { type } = req.query;
  const email = await getEmail(req);
  if (!email) throw new Error("Unauthorized");
  if (!type) {
    if (await validatePermissions(email, [Permissions.VIEW_ALL_GRIEVANCES]))
      return getAllGrievances();
    throw new Error("Unauthorized");
  }
  if (
    (type.toString() === GrTypeType.STAFF &&
      (await validatePermissions(email, [Permissions.VIEW_STAFF_GRIEVANCE]))) ||
    (type.toString() === GrTypeType.STUDENT &&
      (await validatePermissions(email, [
        Permissions.VIEW_STUDENT_GRIEVANCE,
      ]))) ||
    (type.toString() === GrTypeType.INTERNAL &&
      (await validatePermissions(email, [
        Permissions.VIEW_INTERNAL_COMPLAINTS,
      ])))
  )
    return getAllGrievances({type});
  throw new Error("Unauthorized");
});
