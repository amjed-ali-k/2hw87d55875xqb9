import { getAllGrievanceForEmail } from "../../../lib/db/grievances";
import backendGet from "../../../lib/helpers/backendGet";
import { getEmail } from "../../../lib/helpers/validateRoles";

export default backendGet(async (req) => {
  const email = await getEmail(req);
  if (!email) return;
  return getAllGrievanceForEmail(email);
});
