import { getResponse } from "../../../lib/db/grievances";
import backendGet from "../../../lib/helpers/backendGet";
import { getEmail } from "../../../lib/helpers/validateRoles";

export default backendGet(async (req) => {
  const email = await getEmail(req);
  if (!email) return;
  const { id } = req.query;
  const rs = await getResponse(id.toString());
  if (!rs) return;
  if (email === rs.userEmail) {
    return rs;
  }
  throw new Error("Not authorized");
});
