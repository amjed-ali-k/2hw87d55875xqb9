import { GrTypeType } from "../../../lib/constants/grievance";
import { Roles } from "../../../lib/constants/users";
import {
  getAllGrievancesBetweenDates,
  getResponsesForGreivance,
} from "../../../lib/db/grievances";
import { getProfileById } from "../../../lib/db/profile";
import {} from "../../../lib/db/reports";
import backendGet from "../../../lib/helpers/backendGet";
import { getEmail, validateRole } from "../../../lib/helpers/validateRoles";
import { UserType } from "../../../lib/types/db";

export default backendGet(async (req) => {
  const email = await getEmail(req);
  if (!email) throw new Error("You are not allowed for this operation");
  const isAdmin = await validateRole([Roles.ADMIN], req, email);
  if (!isAdmin) throw new Error("Unauthorized");
  const { fromDate, toDate, type } = req.query;
  const grs = await getAllGrievancesBetweenDates(
    new Date(parseInt(fromDate.toString())),
    new Date(parseInt(toDate.toString())),
    type.toString() as GrTypeType
  );
  let users: UserType[] = [];
  const grievances = await Promise.all(
    grs.map(async (g) => {
      const responses = await getResponsesForGreivance(g.key);
      users = await addToArray(g.userId, users);
      for (const resp of responses) {
        users = await addToArray(resp.userId, users);
      }
      return { ...g, responses };
    })
  );
  return { grievances, users };
});

async function addToArray(userId: string, arr: UserType[]) {
  if (arr.find((a) => a.key === userId)) {
    return arr;
  }
  const user = await getProfileById(userId);
  if (user) arr.push(user);
  return arr;
}
