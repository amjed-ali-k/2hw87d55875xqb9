import NextAuth from "next-auth";
import { Adapter } from "next-auth/adapters";
import CredentialsProvider from "next-auth/providers/credentials";
import GitHubProvider from "next-auth/providers/github";
import FacebookProvider from "next-auth/providers/facebook";
import GoogleProvider from "next-auth/providers/google";
import LinkedInProvider from "next-auth/providers/linkedin";
import TwitterProvider from "next-auth/providers/twitter";

import DetaAdapter from "../../../lib/adapters/detaBase";

import { OAuthSettings, SettingsType } from "../../../lib/constants/settings";
import { verifyLogin } from "../../../lib/db/profile";
import { getSettings } from "../../../lib/db/settings";
import { Provider } from "next-auth/providers";
import { NextApiRequest, NextApiResponse } from "next";

export default async function(req:NextApiRequest, res:NextApiResponse) {
  let providers: Provider[] = [];
  const oAuthSettings = await getSettings<OAuthSettings>(SettingsType.OAuth);
  if (oAuthSettings.allowPasswordLogin)
    providers.push(
      CredentialsProvider({
        name: "Email and Password",
        credentials: {
          email: { label: "Email", type: "email", placeholder: "me@email.com" },
          password: { label: "Password", type: "password" },
        },
        async authorize(credentials) {
          const user = await verifyLogin(
            credentials?.email,
            credentials?.password
          );
          if (user) return user;
          return null;
        },
      })
    );

  if (oAuthSettings.facebook.enabled) {
    providers.push(
      FacebookProvider({
        clientId: oAuthSettings.facebook.clientId,
        clientSecret: oAuthSettings.facebook.clientSecret,
      })
    );
  }
  if (oAuthSettings.github.enabled) {
    providers.push(
      GitHubProvider({
        clientId: oAuthSettings.github.clientId,
        clientSecret: oAuthSettings.github.clientSecret,
      })
    );
  }
  if (oAuthSettings.google.enabled) {
    providers.push(
      GoogleProvider({
        clientId: oAuthSettings.google.clientId,
        clientSecret: oAuthSettings.google.clientSecret,
      })
    );
  }
  if (oAuthSettings.linkedin.enabled) {
    providers.push(
      LinkedInProvider({
        clientId: oAuthSettings.linkedin.clientId,
        clientSecret: oAuthSettings.linkedin.clientSecret,
      })
    );
  }
  if (oAuthSettings.twitter.enabled) {
    providers.push(
      TwitterProvider({
        clientId: oAuthSettings.twitter.clientId,
        clientSecret: oAuthSettings.twitter.clientSecret,
      })
    );
  }
  return NextAuth(req, res,{
    // Configure one or more authentication providers
    secret: "vVyswEF5oy/+TsqfOxI5zd7WN0gPTZCC07vhHbr+1wI=",
    providers,
    pages: {
      signIn: "/login",
      signOut: "/",
      newUser: "/post-register",
    },
    session: {
      strategy: "jwt",
    },
    adapter: DetaAdapter(null) as unknown as Adapter,
    debug: true,
    //   callbacks: {
    //     signIn() {
    //       return "/dashboard";
    //     },
    //   },
  });
}

