import React from "react";
import Link from "next/link";
import Image from "next/image";
import { Form, Formik } from "formik";
import * as Yup from "yup";
import toast, { Toaster } from "react-hot-toast";
import { useRouter } from "next/router";

import CheckBox from "../components/Forms/CheckBox";
import TextInput from "../components/Forms/TextInput";
import FormButton from "../components/Forms/FormButton";
import httpClient from "../lib/helpers/httpClient";
import { useFetchPermenantData } from "../lib/adapters/fetcher";
import { Button } from "@windmill/react-ui";
import {
  BsFacebook,
  BsGithub,
  BsGoogle,
  BsLinkedin,
  BsTwitter,
} from "react-icons/bs";
import { signIn } from "next-auth/react";

function CreateAccount() {
  const router = useRouter();
  const { data: providers } = useFetchPermenantData<{ [key: string]: any }>(
    "/api/auth/providers"
  );
  return (
    <div className="flex items-center min-h-screen p-6 bg-gray-50 dark:bg-gray-900">
      <Toaster />
      <div className="flex-1 h-full max-w-4xl mx-auto overflow-hidden bg-white rounded-lg shadow-xl dark:bg-gray-800">
        <div className="flex flex-col overflow-y-auto md:flex-row">
          <div className="relative h-32 md:h-auto md:w-1/2">
            <Image
              src={"/images/sign-up.jpg"}
              alt="New account"
              layout="fill"
              objectFit="cover"
            />
          </div>
          <main className="flex items-center justify-center p-6 sm:p-12 md:w-1/2">
            <div className="w-full">
              <h1 className="mb-4 text-xl font-semibold text-gray-700 dark:text-gray-200">
                Create account
              </h1>
              <Formik
                initialValues={{
                  email: "",
                  password: "",
                  repeatPassword: "",
                  acceptedTerms: false, // added for our checkbox
                }}
                validationSchema={Yup.object({
                  email: Yup.string()
                    .email("Invalid email address")
                    .required("Required"),
                  name: Yup.string()
                    .min(5, "Too Short!")
                    .max(50, "Too Long!")
                    .required("Required"),
                  password: Yup.string()
                    .min(5, "Must be 5 characters or less")
                    .required("Required"),
                  repeatPassword: Yup.string().oneOf(
                    [Yup.ref("password"), null],
                    "Passwords must match"
                  ),
                  acceptedTerms: Yup.boolean()
                    .required("Required")
                    .oneOf([true], "You must accept the terms and conditions."),
                })}
                onSubmit={async (values, { setSubmitting }) => {
                  httpClient
                    .post("/api/auth/create-account/", values)
                    .then((res) => {
                      toast.success("Successfully created!");
                      router.push("/login");
                    })
                    .catch((err) => {
                      toast.error(err.data.error);
                    });
                  setSubmitting(false);
                }}
              >
                <Form>
                  <TextInput
                    label="Name"
                    name="name"
                    type="text"
                    placeholder="Enter your full name"
                  />
                  <TextInput
                    label="Email"
                    name="email"
                    type="text"
                    placeholder="Enter your email"
                  />
                  <TextInput
                    label="Password"
                    name="password"
                    type="password"
                    placeholder="***************"
                  />
                  <TextInput
                    label="Repeat Password"
                    name="repeatPassword"
                    type="password"
                    placeholder="***************"
                  />
                  <CheckBox name="acceptedTerms">
                    I agree to the
                    <span className="underline">privacy policy</span>
                  </CheckBox>

                  <FormButton> Create Account </FormButton>
                </Form>
              </Formik>
              <hr className="mt-8" />
              <p className="my-4 font-bold text-center text-white">
                Or Sign Up with
              </p>
              {providers?.github && (
                <Button
                  className="m-1"
                  onClick={() => {
                    signIn("github", { callbackUrl: "/dashboard" });
                  }}
                  icon={BsGithub}
                >
                  Github
                </Button>
              )}
              {providers?.google && (
                <Button
                  className="m-1"
                  onClick={() => {
                    signIn("google", { callbackUrl: "/dashboard" });
                  }}
                  icon={BsGoogle}
                >
                  Google
                </Button>
              )}
              {providers?.linkedin && (
                <Button
                  className="m-1"
                  onClick={() => {
                    signIn("linkedin", { callbackUrl: "/dashboard" });
                  }}
                  icon={BsLinkedin}
                >
                  LinkedIn
                </Button>
              )}
              {providers?.facebook && (
                <Button
                  className="m-1"
                  onClick={() => {
                    signIn("facebook", { callbackUrl: "/dashboard" });
                  }}
                  icon={BsFacebook}
                >
                  Facebook
                </Button>
              )}
              {providers?.twitter && (
                <Button
                  className="m-1"
                  onClick={() => {
                    signIn("twitter", { callbackUrl: "/dashboard" });
                  }}
                  icon={BsTwitter}
                >
                  Twitter
                </Button>
              )}

              <p className="mt-4">
                <Link href="/login">
                  <a className="text-sm font-medium text-purple-600 dark:text-purple-400 hover:underline">
                    Already have an account? Login
                  </a>
                </Link>
              </p>
            </div>
          </main>
        </div>
      </div>
    </div>
  );
}

export default CreateAccount;
