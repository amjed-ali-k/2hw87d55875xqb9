Sorry! This is private repository

Plans:

- Basic Plan

  - Host: Vercel
  - Database: Deta Base
  - Pricing: Free
  - Setup Charges: 25000 INR
  - Updates: 6 Months
    - Integrations: No
    - Support: Initial Support only
    - Emails: Unavailable

- Premium Plan
  - Host: SSD Nodes
  - Database: Postgres
  - Pricing:
    - $60/month for 1 month
    - $8/month for 3 years | 96$/year | 288$/3 Years
    - $12/month for 1 year | 144$/year | 432$/3 Years
  - Setup Charges: Free
  - Integrations: Telegram Only
  - Updates: Plan Duration
  - Emails: Available
  - Support: Basic
- Pro Plan
  - Host: SSD Nodes
  - Database: Postgres
  - Pricing:
    - $100/month for 1 month
    - $14/month for 3 years | 168$/year | 576$/3 Years
    - $20/month for 1 year | 192$/year | 672$/3 Years
  - Setup Charges: Free
  - Integrations: Whatsapp and Telegram
  - Updates: Plan Duration
  - Emails: Available
  - Support: 24/7 Advanced Support



New Features:
  - Option to assign grievance to member
  - Anonymous grievance submissions
  - OTP Verification for grievance submissions
  - Email Notifications
  - SMS Notifications
  - Customizable grievance categories
  - Unauthorized grievance submission
  - File Appeal againist grievance
  - Sensitive grievance submission



Reports:
  - Count by Categories
  - Count by Status
  