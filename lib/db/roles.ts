import { PermissionsType, RoleType } from "../types/db";
import { getUser, getUserByEmail } from "./auth";
import { permissionsDB, rolesDB } from "./db";
import cache from "memory-cache";
import { getProfileById } from "./profile";

var rolesCache = new cache.Cache();

export async function getRole(id: string) {
  let role = rolesCache.get(id) as RoleType | null;
  if (role) return role;
  role = (await rolesDB.get(id)) as unknown as RoleType | null;
  if (!role) return null;
  rolesCache.put(id, role);
  return role;
}

interface User {
  key: string;
  email: string;
}

export async function addRoleToUser(user: User | string, roles: string[], ) {
  const key = typeof user === "string" ? user : user.key;
  rolesCache.del(key);
  const _roles = await getRole(key) as unknown as RoleType | null;
  let role;
  if (_roles) {
    role = await rolesDB.put({..._roles, roles: [..._roles.roles, ...roles]});
  } else {
    role = await rolesDB.put({ id: key, roles, email: (await getUser(key))?.email}, key);
  }
  if (!role) return null;
  rolesCache.put(key, role);
  return role;
}

export async function removeRoleFromUser(user:User | string, roles: string[]) {
  const key = typeof user === "string" ? user : user.key;
  rolesCache.del(key)
  const _rolesObj = await getRole(key);
  const toRemove = new Set(roles)
  const removed = _rolesObj?.roles.filter(r => !toRemove.has(r))
  if (!removed) return null
  return rolesDB.put({..._rolesObj, roles: removed}, key)
}

export async function deleteRole(id: string) {
  rolesCache.del(id);
  return rolesDB.delete(id);
}

export async function getRoleByEmail(email: string) {
  const user = await getUserByEmail(email);
  if (!user) return null;
  return getRole(user.id);
}

// PERMISSIONS DB FUNCTIONS

var permissionsCache = new cache.Cache();

export async function getPermissions(role: string) {
  let _role = permissionsCache.get(role) as PermissionsType | null;
  if (_role) return _role;
  _role = (await permissionsDB.get(role)) as unknown as PermissionsType;
  if (!_role) return null;
  permissionsCache.put(role, _role);
  return _role;
}

export async function updatePermissions(role: string, permissions: string[]) {
  permissionsCache.del(role);
  const _perm = (await permissionsDB.get(role)) as unknown as PermissionsType;
  if (!_perm) return null;
  const perms_array = Array.from(new Set([..._perm.permissions, ...permissions]))
  const _role = (await permissionsDB.put({key: _perm.key, permissions: perms_array})) as unknown as PermissionsType;
  if (!_role) return null;
  permissionsCache.put(role, _role);
  return _role;
}

export async function addPermission(role: string, permission: string) {
  return updatePermissions(role, [permission]) as unknown as PermissionsType;
}

export async function removePermission(role: string, permission: string) {
  permissionsCache.del(role);
  const _permissions = await getPermissions(role);
  if (!_permissions) return null;
  console.log(permission)
  const permissions = _permissions.permissions.filter((p) => p !== permission);
  console.log(permissions)
  const _pers = (await permissionsDB.put(
    { permissions },
    role
  )) as unknown as PermissionsType;
  permissionsCache.put(role, _pers);
  return _pers;
}

export async function getRoleByEmailWithPermissions(email: string) {
  const role = await getRoleByEmail(email);
  if (!role) return null;
  let permissions: Set<string> = new Set();
  await Promise.all(
    role.roles.map(async (r) => {
      const perm = await getPermissions(r);
      if (perm) perm.permissions.forEach((p) => permissions.add(p));
    })
  );
  return {
    ...role,
    permissions: Array.from(permissions),
  };
}

export async function getUsersByRole(role: string) {
  const userEmails = (await rolesDB.fetch({ "roles?contains": role })).items as
    | RoleType[]
    | null;
  if (!userEmails) return null;
  return Promise.all(
    userEmails.map(async (user) => {
      const profileObj = await getProfileById(user.key);
      const userObj = await getUser(user.key);
      return {...userObj, ...profileObj}
    })
  );
}


