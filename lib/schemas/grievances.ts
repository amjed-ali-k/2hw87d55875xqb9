import * as Yup from "yup";
import { GrievanceSettings } from "../constants/settings";

export const grievanceSchema = {
  title: Yup.string().min(7).required("Required"),
  content: Yup.string().min(15).required("Required"),
  priority: Yup.string().required("Required"),
  category: Yup.string().required("Required"),
  type: Yup.string().required("Required"),
};

export const responseSchema = {
  content: Yup.string().required("Required"),
  status: Yup.string().required("Required"),
  grievanceId: Yup.string().required("Required"),
};

interface Settings {
    contentLengthMax: number;
    contentLengthMin: number;
    responsesLengthMax: number;
    responsesLengthMin: number;
}

export const getGrievanceSchema = (settings: Settings) => {
  return {
    ...grievanceSchema,
    content: Yup.string()
      .min(settings.contentLengthMin, `Must be at least ${settings.contentLengthMin} characters`)
      .max(settings.contentLengthMax, `Must be at most ${settings.contentLengthMax} characters`)
      .required("Required"),  
  }
};


export const getResponseSchema = (settings: Settings) => {
    return {
        ...responseSchema,
        content: Yup.string()
        .min(settings.responsesLengthMin, `Must be at least ${settings.responsesLengthMin} characters`)
        .max(settings.responsesLengthMax, `Must be at most ${settings.responsesLengthMax} characters`)
        .required("Required"),
    } ;
}