import * as Yup from "yup";

export const staffProfileSchema = {
  designation: Yup.string().required("Required"),
  department: Yup.string().required("Required"),
  pen_no: Yup.string().required("Required"),
  institution: Yup.string().required("Required"),
  type: Yup.string().default("staff"),
};

export const studentProfileSchema = {
  department: Yup.string().required("Required"),
  semester: Yup.string().required("Required"),
  year_of_passout: Yup.string().required("Required"),
  register_no: Yup.string().required("Required"),
  institution: Yup.string().required("Required"),
  type: Yup.string().default("student"),
};
export const parentProfileSchema = {
  student_name: Yup.string().required("Required"),
  department: Yup.string().required("Required"),
  register_no: Yup.string().required("Required"),
  semester: Yup.string().required("Required"),
  institution: Yup.string().required("Required"),
  type: Yup.string().default("parent"),
};


