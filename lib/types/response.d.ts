export interface UserGrievanceStatusCount {
    processing: number;
    solved: number;
    rejected: number;
    total: number;
}