/**
 * ⚠ These are used just to render the Sidebar!
 * You can include any link here, local or external.
 *
 * If you're looking to actual Router routes, go to
 * `routes/index.js`
 */

import { IconType } from "react-icons";
import { AiOutlineFileProtect } from "react-icons/ai";
import { BiHomeCircle } from "react-icons/bi";
import { CgBoy } from "react-icons/cg";
import { FaWpforms, FaUserEdit, FaUsers } from "react-icons/fa";
import { HiDocumentReport } from "react-icons/hi";
import { ImProfile } from "react-icons/im";
import { MdOutlineManageAccounts, MdOutlineRememberMe } from "react-icons/md";
import { RiFilePaperLine, RiSettings3Line, RiUser2Fill } from "react-icons/ri";
import { Permissions, Roles } from "./users";

export interface SidebarItem {
  path: string;
  icon: IconType;
  name: string;
  permissions?: Permissions[];
  roles?: Roles[];
  routes?: { path: string; name: string }[];
}

interface TitleItem {
  name: string;
}

type Sidebar = {
  menu: SidebarItem[];
  title: string;
};

const routes: Sidebar[] = [
  {
    title: "My Greivences",
    menu: [
      {
        path: "/dashboard", // the url
        icon: BiHomeCircle, // the component being exported from icons/index.js
        name: "Dashboard", // name that appear in Sidebar
      },
      {
        path: "/grievances",
        icon: FaWpforms,
        name: "My Grievances",
        permissions: [
          Permissions.CREATE_INTERNAL_COMPLAINTS,
          Permissions.CREATE_STUDENT_GRIEVANCE,
          Permissions.CREATE_STAFF_GRIEVANCE,
        ],
      },
    ],
  },
  {
    title: "Other Grievences",
    menu: [
      {
        path: "/grievances/student-grievance/all",
        icon: CgBoy,
        name: "Student Grievances",
        permissions: [Permissions.VIEW_STUDENT_GRIEVANCE],
      },
      {
        path: "/grievances/staff-grievance/all",
        icon: RiUser2Fill,
        name: "Staff Grievances",
        permissions: [Permissions.VIEW_STAFF_GRIEVANCE],
      },
      {
        path: "/grievances/internal-complaints/all",
        icon: RiFilePaperLine,
        name: "Internal Complaints",
        permissions: [Permissions.VIEW_INTERNAL_COMPLAINTS],
      },
    ],
  },
  {
    title: "Configuration",
    menu: [
      {
        path: "/settings/roles",
        icon: MdOutlineRememberMe,
        name: "Members and Roles",
        roles: [Roles.ADMIN],
      },
      {
        path: "/settings/grievances",
        icon: AiOutlineFileProtect,
        name: "Grievance settings",
        roles: [Roles.ADMIN],
      },
      {
        path: "/settings/accounts",
        icon: MdOutlineManageAccounts,
        name: "Account settings",
        roles: [Roles.ADMIN],
      },
      {
        path: "/settings/system",
        icon: RiSettings3Line,
        name: "System settings",
        roles: [Roles.ADMIN],
      },
      {
        path: "/settings/reports",
        icon: HiDocumentReport,
        name: "Reports",
        roles: [Roles.ADMIN],
      },
    ],
  },
  {
    title: "Account details",
    menu: [
      {
        path: "/users/all",
        icon: FaUsers,
        name: "All Users",
        permissions: [Permissions.VIEW_ALL_USER, Permissions.VIEW_STAFF_USER, Permissions.VIEW_STUDENT_USER, Permissions.VIEW_PARENT_USER],
      },
      {
        path: "/users/me",
        icon: ImProfile,
        name: "My Profile",
      },
      {
        path: "/users/edit",
        icon: FaUserEdit,
        name: "Edit Profile",
      },
    ],
  },
];

export default routes;
