// TODO: Fetch this from db
export const category = {
  staff: [
    "Academic issues",
    "Personal issues",
    "Official issues",
    "Other issues",
  ],
  student: ["Infrastructure issues", "other issues"],
  internal: ["Infrastructure issues", "other issues"],
};

export function useStatusField() {
  return [
    "open",
    "solved",
    "processing",
    "rejected",
    "fake",
    "duplicate",
    "less_info",
  ];
}

export enum Statuses {
  OPEN = "open",
  SOLVED = "solved",
  PROCESSING = "processing",
  REJECTED = "rejected",
  FAKE = "fake",
  DUPLICATE = "duplicate",
  LESS_INFO = "less_info",
}

export enum GrTypeType {
  STUDENT = "student",
  STAFF = "staff",
  INTERNAL = "internal",
}

export const ClosedStatuses = [
  Statuses.SOLVED,
  Statuses.DUPLICATE,
  Statuses.REJECTED,
  Statuses.LESS_INFO,
  Statuses.FAKE,
];
