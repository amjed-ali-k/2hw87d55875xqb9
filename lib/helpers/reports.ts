import { Statuses } from "../constants/grievance";
import { grievanceDB } from "../db/db";
import { GrievanceType } from "../types/db";

export async function getMonthlyGrievances(month: number, year: number) {
  console.log("Fetching monthly grievances...");
  const start = new Date(year, month, 1);
  const end = new Date(year, month + 1, 0);
  return getCreatedAt(start.getTime(), end.getTime());
}

export async function getYearlyGrievances(year: number) {
  console.log("Fetching yearly grievances...");
  const start = new Date(year, 0, 1);
  const end = new Date(year + 1, 0, 0);
  return getCreatedAt(start.getTime(), end.getTime());
}

export async function getMonthlyClosedGrievances(month: number, year: number) {
  const start = new Date(year, month, 1);
  const end = new Date(year, month + 1, 0);
  return getClosedAt(start.getTime(), end.getTime());
}

export async function generateMonthlyGrievance(month: number, year: number) {
  console.log("Generating monthly grievances...");
  let result: any;
  const grss = await getMonthlyGrievances(month, year);
  result.total = grss.length;
  Object.values(Statuses).forEach((status) => {
    result[status] = grss.filter((gr) => gr.status === status).length;
  });
  result.pinned = grss.filter((gr) => gr.pinned).length;
  result.unpinned = grss.filter((gr) => !gr.pinned).length;
  return result;
}

export async function getClosedAt(start: number, end: number) {
  return (await grievanceDB.fetch({ "updatedAt?r": [start, end] }))
    .items as unknown as GrievanceType[];
}

export async function getCreatedAt(start: number, end: number) {
  return (await grievanceDB.fetch({ "createdAt?r": [start, end] }))
    .items as unknown as GrievanceType[];
}

