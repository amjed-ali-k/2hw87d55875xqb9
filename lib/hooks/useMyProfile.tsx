import { useRouter } from "next/router";
import { useSWRConfig } from "swr";
import { useFetchPermenantData } from "../adapters/fetcher";
import { UserProfileFullType } from "../types/db";

export function useMyProfile(refresh: boolean = false) {
  const { mutate } = useSWRConfig();
  if (refresh) mutate("/api/users/profile/me");
  const { data, error } = useFetchPermenantData<UserProfileFullType>(
    () => `/api/users/me`
  );
  const router = useRouter();
   if (router.pathname !== "/post-register") {
  if (data && !data.type) router.push("/post-register");
  if (error && error.status === 406) router.push("/post-register");}
  if (error && error.status === 401) router.push("/login");
  return data;
}
